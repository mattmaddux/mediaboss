//
//  UserDefaultsKeys.swift
//  MediaBoss
//
//  Created by Matthew Maddux on 2/12/16.
//  Copyright © 2016 MadduxApps. All rights reserved.
//

import Foundation

struct UserDefaultsKeys {
    static let MoviesURL: String = "moviesURL"
    static let ShowsURL: String = "showsURL"
    static let TorrentURL: String = "torrentURL"
}
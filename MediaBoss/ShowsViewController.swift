//
//  ShowsViewController.swift
//  MediaBoss
//
//  Created by Matthew Maddux on 2/12/16.
//  Copyright © 2016 MadduxApps. All rights reserved.
//

import UIKit

class ShowsViewController: UIViewController {
    
    // ======================================================= //
    // MARK: - Private Properties
    // ======================================================= //
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    
    // ======================================================= //
    // MARK: - Public Properties
    // ======================================================= //
    
    var url: NSURL?
    var sickRage = SickRage.sharedInstance
    
    
    // ======================================================= //
    // MARK: - UI Outlets
    // ======================================================= //
    
    @IBOutlet weak var webView: UIWebView!
    
    
    // ======================================================= //
    // MARK: - Private Methods
    // ======================================================= //
    
    private func updateURL() {
        url = defaults.URLForKey(UserDefaultsKeys.ShowsURL)
    }
    
    private func reloadWebView() {
        if let confirmedURL = url {
            let request = NSURLRequest(URL: confirmedURL)
            webView.loadRequest(request)
        }
    }
    
    // ======================================================= //
    // MARK: - Lifecycle Methods
    // ======================================================= //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateURL()
        reloadWebView()
        sickRage.APIKey = "46b36581595e54d58e7889d63802d0b6"
        sickRage.ServerURL = String(url!)
//        sickRage.get(Show: 77398, Season: 10, Episode: 6)
        sickRage.getShows()
    }
    
    override func viewDidAppear(animated: Bool) {
        if url != defaults.URLForKey(UserDefaultsKeys.ShowsURL) {
            updateURL()
            reloadWebView()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

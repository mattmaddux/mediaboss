//
//  SickRage.swift
//  MediaBoss
//
//  Created by Matthew Maddux on 2/17/16.
//  Copyright © 2016 MadduxApps. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire


class SickRage {
    
    // ======================================================= //
    // MARK: - Shared App Instance
    // ======================================================= //
    
    static let sharedInstance = SickRage()
    
    // ======================================================= //
    // MARK: - Private Properties
    // ======================================================= //
    
    private let fetcher = JSONFetcher()
    private let manager = Alamofire.Manager
    
    // ======================================================= //
    // MARK: - Public Properties
    // ======================================================= //
    
    var APIKey: String?
    var ServerURL: String?
    
    // ======================================================= //
    // MARK: - Private Methods
    // ======================================================= //
    
    func get(Show show: Int, Season s: Int, Episode e: Int) {
        if APIKey != nil && ServerURL != nil {
            let requestString = String(format: "%@%@%@%@episode&tvdbid=%d&season=%d&episode=%d&full_path=1",
                arguments: [ServerURL!, URLComponents.API, APIKey!, URLComponents.CMD, show, s, e])
            let requestURL = NSURL(string: requestString)
            fetcher.get(fromURL: requestURL!) { json in
                print(json)
            }
        }
    }
    
    func getShows() {
        if APIKey != nil && ServerURL != nil {
            let requestString = ServerURL! + URLComponents.API + APIKey! + "/"
            let params = ["cmd":"shows"]
            Alamofire.request(.GET, requestString, parameters: params)
                .responseJSON { response in
                    print(response.request)
                    print(response.response)
                    print(response.data)
                    print(response.result)
                    
                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                    }
            }
        }
    }
    
    // ======================================================= //
    // MARK: - SickRage Model Classes
    // ======================================================= //
    
    struct Show {
        let id: Int
        let seasonList: [Int]
        let name: String
        let poster: UIImage?
    }
    
    struct Season {
        let seasonNumber: Int
        let episodes = [Episode]()
    }
    
    struct Episode {
        let airdate: NSDate
        let location: String
        let name: String
        let status: Status
        let quality: Quality?
        let releaseName: String?
    }
    
    struct Command {
        let name: String
        let params: [String]
        let description: String
        
    }
    
    // ======================================================= //
    // MARK: - Constants
    // ======================================================= //
    
    struct URLComponents {
        static let API = "/api/"
        static let CMD = "/?cmd="
    }
    
    // ======================================================= //
    // MARK: - SickRage Enums
    // ======================================================= //
    
    enum Status {
        case Unaired
        case Wanted
        case Skipped
        case Archived
        case Ignored
        case Failed
        case Downloaded
    }
    
    enum Quality: String {
        case SDTV = "SDTV"
        case SDDVD = "SD DVD"
        case RAWHD = "RawHD"
        case HDTV = "HDTV"
        case HDTV1080 = "1080p HDTV"
        case HDTV720 = "720p HDTV"
        case WEBDL1080 = "1080p WEB-DL"
        case WEBDL720 = "720p WEB-DL"
    }
}


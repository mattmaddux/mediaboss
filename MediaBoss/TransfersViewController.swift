//
//  TransfersViewController.swift
//  MediaBoss
//
//  Created by Matthew Maddux on 2/12/16.
//  Copyright © 2016 MadduxApps. All rights reserved.
//

import UIKit

class TransfersViewController: UIViewController {
    
    // ======================================================= //
    // MARK: - Private Properties
    // ======================================================= //
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    
    // ======================================================= //
    // MARK: - Public Properties
    // ======================================================= //
    
    var url: NSURL?
    
    
    // ======================================================= //
    // MARK: - UI Outlets
    // ======================================================= //
    
    @IBOutlet weak var webView: UIWebView!
    
    
    // ======================================================= //
    // MARK: - Private Methods
    // ======================================================= //
    
    private func updateURL() {
        url = defaults.URLForKey(UserDefaultsKeys.TorrentURL)
    }
    
    private func reloadWebView() {
        if let confirmedURL = url {
            let request = NSURLRequest(URL: confirmedURL)
            webView.loadRequest(request)
        }
    }
    
    // ======================================================= //
    // MARK: - Lifecycle Methods
    // ======================================================= //

    override func viewDidLoad() {
        super.viewDidLoad()
        updateURL()
        reloadWebView()
    }
    
    override func viewDidAppear(animated: Bool) {
        if url != defaults.URLForKey(UserDefaultsKeys.TorrentURL) {
            updateURL()
            reloadWebView()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

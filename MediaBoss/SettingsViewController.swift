//
//  SettingsViewController.swift
//  MediaBoss
//
//  Created by Matthew Maddux on 2/12/16.
//  Copyright © 2016 MadduxApps. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITextFieldDelegate {
    
    // ======================================================= //
    // MARK: - Private Properties
    // ======================================================= //
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    
    // ======================================================= //
    // MARK: - UI Outlets
    // ======================================================= //
    
    @IBOutlet weak var couchPotatoField: UITextField!
    @IBOutlet weak var sickRageField: UITextField!
    @IBOutlet weak var torrentField: UITextField!
    
    
    // ======================================================= //
    // MARK: - UITextField Delegate
    // ======================================================= //
    
    func textFieldDidEndEditing(textField: UITextField) {
        textField.resignFirstResponder()
        updateUserDefaults()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    // ======================================================= //
    // MARK: - Private Methods
    // ======================================================= //
    
    private func updateUserDefaults() {
        setURL(couchPotatoField.text, forKey: UserDefaultsKeys.MoviesURL)
        setURL(sickRageField.text, forKey: UserDefaultsKeys.ShowsURL)
        setURL(torrentField.text, forKey: UserDefaultsKeys.TorrentURL)
    }
    
    private func setURL(urlString: String?, forKey key: String) {
        if let confirmedURLString = urlString {
            let url = NSURL(string: confirmedURLString)
            defaults.setURL(url, forKey: key)
        }
    }
    
    // ======================================================= //
    // MARK: - Lifecycle Methods
    // ======================================================= //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        couchPotatoField.delegate = self
        couchPotatoField.text = defaults.URLForKey(UserDefaultsKeys.MoviesURL)?.description
        sickRageField.delegate = self
        sickRageField.text = defaults.URLForKey(UserDefaultsKeys.ShowsURL)?.description
        torrentField.delegate = self
        torrentField.text = defaults.URLForKey(UserDefaultsKeys.TorrentURL)?.description
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
